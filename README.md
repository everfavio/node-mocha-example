# node-mocha-example

### Algorithms implemented
#### Client Side
1. Prime numbers checker
2. Prime factors finder
3. Fibonacci series calculator
4. The Greatest Common Divisor
5. Duplicates remover
6. Merge of two sorted arrays
7. Strings reverser
8. Words counter
9. Tags collector
10. The first non-repeating char

#### Server Side
1. Server Status Checker
2. Server-side Profile Constructor

### Setting up of the environment
In the directory for the project we should type the next command:
```
npm init
```

As a result we have to get a `package.json` file for our project.

Next, we have to install our testing framework, and an expectation library called Chai that serves as a nice replacement for Node's standard assert function:
```
npm install mocha --save
npm install chai --save
```
To be able to use server-side testing, you will need this package. 
```
npm install request --save
```
Also, make sure that server is running, in order to use the server-side tests.


Finaly, the following command is used to invoke the Mocha binary installed locally in the `./node_modules` directory:
```
./node_modules/.bin/mocha --reporter spec
```

